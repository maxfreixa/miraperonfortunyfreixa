<?php

/*
 * Aquest controlador ha de controlar quin resultat li mostrarem a l'usuari segons
 * d'on rep el control, és a dir, si el rep des de joc, des d'una resposta seleccionada,
 * des de l'opció de sortir o bé des de l'inici.
 * En el cas de sortir, a l'usuari se li ha de tornar a mostrar la pàgina d'inici i tancar la sessió.
 * En els altres casos el controlador ha de cridar a l'script corresponent de model 
 * per gestionar la vista per després mostrar-li de nou el portal amb la vista gestionada.
 */
include("vista/portal.php");
include("controlador/controladorJoc.php");
include("controlador/controladorResposta.php");
include("model/gestioVistaJoc.php");
if(isset($_POST["sortir"])){
    $novaSessio->tancarSessio();
}else if(isset($_POST["sortir"])){
    include("model/gestioVistaJoc.php");
}else if($opcio==2){
    //include("../model/gestio")
}
?>
