<?php
/*
 * Aquest controlador ha de generar la pregunta i possibles respostes que li mostrarem
 * a l'usuari en portal.php.
 * Com heu vist, les preguntes del joc es guarden en el vector de preguntes de la
 * classe joc, per tant la pregunta que li mostrem a l'usuari l'hem de seleccionar 
 * d'aquest vector. La selecció de la pregunta l'heu de fer de manera aleatòria mitjançant
 * el mètode corresponent de la classe Joc.
 * Com ja sabeu, la pregunta té associades 4 possibles respostes, on una de les 4 és la verdadera.
 * Aquestes són les respostes que li haureu de mostrar a l'usuari. Perquè aquestes
 * respostes no es mostrin sempre amb el mateix ordre, abans de mostrar-li les haurem
 * de barrejar mitjançant el mètode escaient de la classe Pregunta.
 * De moment, com heu vist, nosaltres encara no sabem afegir preguntes al joc, per tant,
 * per provar que tot funciona bé, heu de crear un conjunt de preguntes amb les seves respostes
 * i afegir-les a les preguntes del joc.
 * En aquest cas aquest resultat (pregunta més respostes) correspon a l'opció 3 de portal.php.
 * Heu de guardar la pregunta, l'enunciat de la pregunta, respostes i opció en la sessió mitjançant les variables "pregunta", "enunciat"
 * "respostes" i "opcio".
 * Utilitzeu els mètodes necessaris de les classes creades.
 * Penseu  que un cop guardades les dades necessàries en la sessió, passarem el control
 * de l'aplicació a controladorPortal.
 */
include("../model/gestioVistaJoc.php");
?>