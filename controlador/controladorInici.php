<?php
/*
 * Aquest controlador ha de controlar si l'usuari ha seleccionat jugar o afegir
 * pregunta.
 * Si ha seleccionat jugar, es guardarà l'opció 1 i el missatge "Estàs jugant",
 * en la sessió  mitjançant les variables "opcio" i "missatge".
 * Si ha seleccionat afegir pregunta, es guardarà l'opció 2 i el missatge "Estàs afegint una pregunta",
 * en la sessió mitjançant les variables "opcio" i "missatge".
 * Si accedim per primer cop a l'aplicació, mostrarem la pàgina inici.php 
 * Utilitzeu els mètodes necessaris de les classes creades.
 * Penseu  que un cop guardades les dades necessàries en la sessió, passarem el control
 * de l'aplicació a controladorPortal.
 */


//include("../vista/inici.php");
//require_once(dirname(__FILE__) . "/vista/inici.php");

require_once("vista/inici.php");
require_once("model/classes/Sessio.php");

$novaSessio = new Sessio();
if(isset($_POST["jugar"])){
    //include("controlador/controladorPortal.php");
    include("vista/portal.php");
    echo "2";    
    $opcio = $novaSessio->afegirContingutSessio("opcio","1");
    $missatge = $novaSessio->afegirContingutSessio("missatge","Estàs jugant");
}else if(isset($_POST["afegirPregunta"])){
    include("controlador/controladorPortal.php");
    echo "3";
    $opcio = $novaSessio->afegirContingutSessio($opcio,"2");
    $missatge = $novaSessio->afegirContingutSessio($missatge,"Estàs afegint una pregunta");
}
?>