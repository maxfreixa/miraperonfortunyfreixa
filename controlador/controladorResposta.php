<?php
/*
 * Aquest controlador ha de controlar quin resultat li mostrarem a l'usuari segons la 
 * resposta seleccionada en la pàgina portal.php.
 * Si la resposta seleccionada és la verdadera, el resultat que li mostrarem a l'usuari
 * serà "Mira per on!, has encertat la resposta", en cas contrari "Mira per on!, no has encertat la resposta".
 * Aquestes respostes han d'anar acompanyades d'una imatge que seleccionareu vosaltres i
 * que s'han de guardar al directori d'imatges de l'aplicació.
 * En aquest cas, aquest resultat correspon a l'opció 4 de portal.php
 * Heu de guardar la resposta, la URL de la imatge i l'opció corresponent en la sessió mitjançant les
 * variables "resposta", "imatge" i "opcio".
 * Utilitzeu els mètodes necessaris de les classes creades.
 * Penseu  que un cop guardades les dades necessàries en la sessió, passarem el control
 * de l'aplicació a controladorPortal.
 */

?>
