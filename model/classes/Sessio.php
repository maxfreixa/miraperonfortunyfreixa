<?php

class Sessio {

//Constructor
    function __contruct(){
        session_start();
    }

////Paràmaetres: cap
//Funcionalitat:inici de sessió session_start()

//Funció que destrueix una sessió o sessions creades en un script
    function destruirSessio() {
        session_destroy();
    }

//Funció que afegeixi un contingut a un index determinat de $_SESSION.
//Tant l'index com el contingut s'han de passar com a paràmetres.
    function afegirContingutSessio($index, $contingut) {
        $_SESSION[$index]=$contingut;
    }

//Funció que afegeixi un contingut al final d'un vector què és el contingut d'un
//dels index de $_SESSION. Tant l'index com el contingut s'han de passar com a 
//paràmetres.
   function afegirContingutVectorSessio($index, $contingut) {
        $_SESSION[$index][] = $contingut;
       
   }

//Funció que retorna el contingut de $_SESSION per l'index passat com a paràmetre. 
//Si no hi ha missatge guardat retorna null.
    function llegirContingutSessio($index) {
        if (isset($_SESSION[$index])) {
            return $_SESSION[$index];
        }else{
            return "";
        }
    }

//Funció per destruir una sessió, buidar la variable $_SESSION.
    function tancarSessio() {
        session_destroy();
        $_SESSION=array();        
    }
    
}
?>
