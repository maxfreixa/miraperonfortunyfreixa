<?php

/*
 * Un Joc es defineix per un vector de preguntes
 */

/**
 * Description of Joc
 *
 * @author cesca
 */

include("classes/Pregunta.php");

class Joc {

    //Atribut:
    // $preguntes -> vector de preguntes
    private $preguntes;
    //Constructor
    //Paràmetres: cap
    //Funcionalitat:inicialització a buit el vector de preguntes 
    function __construct(){
        $this->preguntes = array();
    }

    //mètodes accessors
    

    //Funció que ha d'afegir la pregunta passada per paràmetre al final del vector de preguntes
    function afegirPregunta(Pregunta $pregunta) {
        array_push($this->preguntes, $pregunta);
    }
    
    //Funció que seleccionarà aleatòriament una pregunta de les preguntes i la retornarà.
    function seleccionarPregunta(){
        return $this->preguntes[] = array_rand($this->preguntes);
    }

}
