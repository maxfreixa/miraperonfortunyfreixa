<?php

/*
 * Una pregunta es defineix per un codi que generarà automàticament el constructor,
 * l'enunciat de la pregunta (la pregunta en si), un vector amb les 4 respostes possibles
 * i la categoria a la qual pertany.
 */

/**
 * Description of Pregunta
 *
 * @author cesca
 */
include("Resposta.php");
class Pregunta {
    
    //Atributs:
    //$codi -> codi de la resposta que es generà automàticament
    private $codi;
    //$proximCodi -> atribut estàtic que indicara quin serà el valor de la pròxima resposta.
    //               L'inicialitzarem a 1.
    private static $proximCodi=1;
    //$enunciat -> cadena que correspon a l'enunciat de la pregunta.
    private $enunciat;
    //$respotes -> vector amb les respostes de la pregunta.
    private $respostes = array();
    //$categoria -> categoria a la que pertany la pregunta.
    private $categoria;   
    //Constructor
    ////Paràmetres: $enunciat i $categoria
    function __construct($enunciat,$categoria){
        $this->enunciat=$enunciat;
        $this->categoria=$categoria;
        $this->codi = $this->proximCodi;
        $this->proximCodi++;
    }
    //Funcionalitat: inicialització dels atributs amb els valors passats per paràmetre
    //menys en el cas de:
    // - codi que s'inicialitzarà amb el valor del pròxim codi per després augmentar
    //   en 1 el valor de proximCodi.
    // - proximCodi que, com hem vist abans, augmentarà el seu valor en 1 per generar
    //   els codis de les preguntes automàticament.
    // - respostes l'inicialitzarem com un vector buit.
    
    //mètodes accessors, menys els de proximCodi
   

    //Funció que ha d'afegir la resposta passada per paràmetre al final del vector de respostes.
    //El control per afegir només 4 respostes i que solament 1 sigui verdadera, ho farem mitjançant
    //l'aplicació, en la pròxima UF.
    function afegirResposta(Resposta $resposta) {
        $this->respotes [$resposta][] = $resposta;        
    }
    
    //Funció que barrejarà les 4 respostes del vector de respostes.
    function barrejarRespostes(){
        shuffle($this->respostes);  
    }
}
