<?php

/* 
 *Script que:
 *  -Llegeix la resposta guardada a la sessió
 *  -Llegeix el missatge guardat a la sessió
 *  -Llegeix la URL de la imatge guardada a la sessió
 *  -Llegeix l'opció guardad a la sessió.
 *  -Assigna els resultats llegits a les variables corresponents de la vista.
 *Utilitzeu els mètodes necessaris de les classes creades.
 */
include("classes/Sessio.php");
include("classes/Resposta.php");
include("classes/Pregunta.php");
include("classes/Joc.php");
$novaSessio = new Sessio();

$novaPregunta = new Pregunta();

$nouJoc = new Joc();

$novaResposta = new Resposta();



?>