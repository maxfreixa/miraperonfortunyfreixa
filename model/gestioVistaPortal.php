<?php

/* 
 *Script que:
 *  -Llegeix el missatge guardat a la sessió.
 *  -Llegeix l'opció guardada a la sessió
 *  -Assigna els resultats llegits a les variables corresponents de la vista.
 *Utilitzeu els mètodes necessaris de les classes creades.
 */
include("classes/Sessio.php");
include("classes/Resposta.php");
include("classes/Pregunta.php");
include("classes/Joc.php");

?>