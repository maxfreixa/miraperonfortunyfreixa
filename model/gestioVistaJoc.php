<?php

/* 
 *Script que:
 *  -Llegeix l'a pregunta'enunciat de la pregunta guardat a la sessió
 *  -Llegeix les respostes de les preguntes guardades a la sessió
 *  -Llegeix el missatge guardat a la sessió
 *  -Llegeix l'opció guardada a la sessió 
 *  -Assigna els resultats llegits a les variables corresponents de la vista.
 *Utilitzeu els mètodes necessaris de les classes creades.
 */
include("classes/Sessio.php");
include("classes/Resposta.php");
include("classes/Pregunta.php");
include("classes/Joc.php");
$pregunta1 = new Pregunta("Com es diu el director de Kill Bill","cinema");
$resposta1 = new Resposta("Quentin Tarantino",false);
$nouJoc = new Joc();
$nouJoc->afegirPregunta($pregunta1);
 
?>